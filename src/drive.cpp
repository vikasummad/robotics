#include "main.h"

using namespace okapi;

std::shared_ptr<ChassisController> driveChassis;

std::unique_ptr<Motor> r1, r2, l1, l2;

std::unique_ptr<ADIEncoder> LEnc, REnc;

PidController drivePIDL1 = pidControllerCreate(0.5, 0, 0.2);
PidController drivePIDL2 = pidControllerCreate(0.5, 0, 0.2);
PidController drivePIDR1 = pidControllerCreate(0.5, 0, 0.2);
PidController drivePIDR2 = pidControllerCreate(0.5, 0, 0.2);

namespace drive {

  void init() {

    r1 = std::make_unique<Motor>(DRIVE_RIGHT_1, false,
                                AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);

    r2 = std::make_unique<Motor>(DRIVE_RIGHT_2, false,
                                AbstractMotor::gearset::green,  AbstractMotor::encoderUnits::degrees);

    l1 = std::make_unique<Motor>(DRIVE_LEFT_1, false,
                               AbstractMotor::gearset::green,  AbstractMotor::encoderUnits::degrees);

    l2 = std::make_unique<Motor>(DRIVE_LEFT_2, false,
                                AbstractMotor::gearset::green,  AbstractMotor::encoderUnits::degrees);

    driveChassis = ChassisControllerBuilder()
                       .withMotors({DRIVE_LEFT_1, DRIVE_LEFT_2}, {-DRIVE_RIGHT_1, -DRIVE_RIGHT_2})
                       //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                       .withDimensions(AbstractMotor::gearset::green,{{3.25_in, 9.625_in}, imev5GreenTPR})
                       .withGains(
                         {0.001, 0, 0.0001},
                         {0.001, 0, 0.0001}
                       )
                       .build();

     //okapiOdom = std::make_unique<Odometry>(TimeUtilFactory::withSettledUtilParams(50, 5, 100_ms), driveChassis->getModel(), {2.841_in, 7.3_in});



     LEnc = std::make_unique<ADIEncoder>(ENC_LEFT_1, ENC_LEFT_2, true);
     REnc = std::make_unique<ADIEncoder>(ENC_RIGHT_1, ENC_RIGHT_2, false);

  }


  void setDrivePowLR(int L, int R) {
    r1->moveVoltage(R);
    r2->moveVoltage(R);
    l1->moveVoltage(L);
    l2->moveVoltage(L);
  }

  double powToVolts(float power) {
    return (power / 127.0) * 12000.0;
  }

  void setDrivePowAll(int l1p, int l2p, int r1p, int r2p) {
    r1->moveVoltage(r1p);
    r2->moveVoltage(r2p);
    l1->moveVoltage(l1p);
    l2->moveVoltage(l2p);
  }

  void drivePIDLR(float LTarg, float RTarg, int t) {
    int temp = 0;
    while(temp < t ) {
      float l1pow = powToVolts(pidControllerComputeOutput(&drivePIDL1, (LTarg - l1->getPosition()) ));
      float l2pow = powToVolts(pidControllerComputeOutput(&drivePIDL2, (LTarg - l2->getPosition()) ));
      float r1pow = powToVolts(pidControllerComputeOutput(&drivePIDR1, (RTarg - r1->getPosition()) ));
      float r2pow = powToVolts(pidControllerComputeOutput(&drivePIDR2, (RTarg - r2->getPosition()) ));

      printf("ERROR: %lf, %lf, %lf, %lf\n", LTarg - l1->getPosition(), LTarg - l2->getPosition(), RTarg - r1->getPosition(), RTarg - r2->getPosition());
      printf("POWER: %lf, %lf, %lf, %lf\n", l1pow, l2pow, r1pow, r2pow);
      setDrivePowAll(l1pow, l2pow, -r1pow, -r2pow);
      temp++;
      pros::delay(5);
    }
    setDrivePowAll(0, 0, 0, 0);

  }

  double getLeftEnc() {
    return (l1->getPosition() + l2->getPosition()) / 2.0;
  }

  double getRightEnc() {
    return (r1->getPosition() + r2->getPosition()) / 2.0;
  }

  void stopAll() {
    setDrivePowLR(0,0);
  }

  void tareAllDrive() {
    r1->tarePosition();
    r2->tarePosition();
    l1->tarePosition();
    l2->tarePosition();
  }

  void setDriveMaxCurrent(int ilimit) {
    l1->setCurrentLimit(ilimit);
    l2->setCurrentLimit(ilimit);
    r1->setCurrentLimit(ilimit);
    r2->setCurrentLimit(ilimit);
  }

  void userLR(double iforwardSpeed, double iyaw, double ithreshold) {
    double forwardSpeed = std::clamp(iforwardSpeed, -1.0, 1.0);
      if (std::abs(forwardSpeed) <= ithreshold) {
        forwardSpeed = 0;
      }

      double yaw = std::clamp(iyaw, -1.0, 1.0);
      if (std::abs(yaw) <= ithreshold) {
        yaw = 0;
      }

      double maxInput = std::copysign(std::max(std::abs(forwardSpeed), std::abs(yaw)), forwardSpeed);
      double leftOutput = 0;
      double rightOutput = 0;

      if (forwardSpeed >= 0) {
        if (yaw >= 0) {
          leftOutput = maxInput;
          rightOutput = forwardSpeed - yaw;
        } else {
          leftOutput = forwardSpeed + yaw;
          rightOutput = maxInput;
        }
      } else {
        if (yaw >= 0) {
          leftOutput = forwardSpeed + yaw;
          rightOutput = maxInput;
        } else {
          leftOutput = maxInput;
          rightOutput = forwardSpeed - yaw;
        }
      }

      setDrivePowLR(static_cast<int16_t>(std::clamp(leftOutput, -1.0, 1.0) * 12000), static_cast<int16_t>(std::clamp(rightOutput, -1.0, 1.0) * 12000));

      std::cout << r1->getActualVelocity()<< " " << r2->getActualVelocity() << " " << l1->getActualVelocity() << " " << l2->getActualVelocity() << std::endl;


  }



  int32_t getLeftVoltage() {
    return l2->getVoltage();
  }

  int32_t getRightVoltage() {
    return r2->getVoltage();
  }

  int32_t getCurrentDraw() {
    return (r2->getCurrentDraw() + l2->getCurrentDraw()) / 2.0;
  }

  double getVelocity() {
    return (std::abs(r2->getActualVelocity()) + std::abs(l2->getActualVelocity())) / 2.0;
  }

}
