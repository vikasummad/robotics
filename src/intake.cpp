#include "main.h"

using namespace okapi;

std::unique_ptr<pros::Motor> intake_r, intake_l;
std::unique_ptr<pros::ADIAnalogIn> lineSense;
int intakeSpeed = 0;

namespace roller {

  int highSpeed = 12000;

  int lowSpeed = 6000;

  int dispenseSpeed = 5000;

  int speed = highSpeed;

  void init() {
    intake_r = std::make_unique<pros::Motor>(INTAKE_R,
                                       pros::E_MOTOR_GEARSET_18, true);

    intake_l = std::make_unique<pros::Motor>(INTAKE_L,
                                       pros::E_MOTOR_GEARSET_18, false);

    lineSense = std::make_unique<pros::ADIAnalogIn>(LINE_SENSOR);
  }

  void stop() {
    intake_r->move_voltage(0);
    intake_l->move_voltage(0);
  }

  void in(double speed) {
    intake_r->move_voltage(speed);
    intake_l->move_voltage(speed);
  }

  void out(double speed) {
    intake_r->move_voltage(-speed);
    intake_l->move_voltage(-speed);
  }

  void intakeControl(void *param) {
    intakeSpeed = highSpeed;
    while(!pros::competition::is_autonomous()) {

      if(!master.getDigital(ControllerDigital::L2)) {

        if(master.getDigital(ControllerDigital::R1)) {
          in(intakeSpeed);
          while(master.getDigital(ControllerDigital::R1)) {
            pros::delay(10);
          }
          stop();
        }

        else if(master.getDigital(ControllerDigital::R2)) {
          out(intakeSpeed);
          while(master.getDigital(ControllerDigital::R2)) {
            pros::delay(10);
            //printf("L: %lf \t R:%lf\n", intake_l->getTorque(), intake_r->getTorque());

          }
          stop();
        }
      }

      if(dispensing) {
        //printf("reached\n");
        while(dispensing) {
          //printf("in loop\n");
          intake_r->move(30);
          intake_l->move(30);
          if((master.getDigital(ControllerDigital::R1) || master.getDigital(ControllerDigital::R2) || master.getDigital(ControllerDigital::L2) || master.getDigital(ControllerDigital::L1)))
            break;
          pros::delay(10);
        }
        stop();
      }

      if(master.getDigital(ControllerDigital::A)) {
        if(intakeSpeed == highSpeed)
          intakeSpeed = lowSpeed;
        else
          intakeSpeed = highSpeed;

        pros::delay(500);
      }
/*
      if(master.getDigital(ControllerDigital::X)) {
        int minTimer = 0;
        while(!(master.getDigital(ControllerDigital::R1) || master.getDigital(ControllerDigital::R2) || master.getDigital(ControllerDigital::L2) || master.getDigital(ControllerDigital::L1))) {
          intake_l->move(127);
          intake_r->move(127);
          if((intake_l->get_torque() > 0.3 || intake_r->get_torque() > 0.3) && minTimer > 20) {
            intake_r->move(0);
            intake_l->move(0);
            break;
          }
          minTimer++;
          pros::delay(10);
        }
      }
*/
      if((master.getDigital(okapi::ControllerDigital::L2) && master.getDigital(okapi::ControllerDigital::B)) || (master.getDigital(okapi::ControllerDigital::L2) && master.getDigital(okapi::ControllerDigital::down))) {
        //printf("reached\n");
        pros::delay(500);
        intake_r->move(-127);
        intake_l->move(-127);
        pros::delay(300);
        stop();
      }
      //printf("%d\n", lowMacroRollers);
      //printf("L: %lf \t R:%lf\n", intake_l->getTorque(), intake_r->getTorque());

      if(master.getDigital(ControllerDigital::left)) {
        while(!(master.getDigital(ControllerDigital::R1) || master.getDigital(ControllerDigital::R2) || master.getDigital(ControllerDigital::L2) || master.getDigital(ControllerDigital::L1))) {
          intake_r->move(30);
          intake_l->move(30);
          pros::delay(10);
        }
        stop();
      }

      if(master.getDigital(ControllerDigital::up)) {
        int originalSense = lineSense->get_value();
        while(!(master.getDigital(ControllerDigital::R1) || master.getDigital(ControllerDigital::R2) || master.getDigital(ControllerDigital::L2) || master.getDigital(ControllerDigital::L1))) {
          intake_r->move(-70);
          intake_l->move(-70);
          //printf("%d  %d\n", lineSense->get_value(), (lineSense->get_value() - originalSense));
          if((lineSense->get_value() - originalSense) < -20) break;
          pros::delay(10);
        }
        intake_r->move(0);
        intake_l->move(0);
      }


      if(liftm->get_position() > 1700) {
        intakeSpeed = 8000;
      }
      else{
        intakeSpeed = highSpeed;
      }
      pros::delay(10);
    }

  }



  double getTempRight() {
    return intake_r->get_temperature();
  }

  double getTempLeft() {
    return intake_l->get_temperature();
  }

} // namespace roller
