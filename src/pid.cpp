#include "main.h"

using namespace okapi;

double limUpTo(double limit, double valueToCheck) {
  if (valueToCheck > limit)   return limit;
  return valueToCheck;
}

double maxSpeedLimit(double maxSpeed, double currSpeed) {
  if(abs(currSpeed) > abs(maxSpeed)) return maxSpeed;
  return currSpeed;
}

PidController pidControllerCreate(double kP, double kI, double kD) {
  return (PidController) {.kP = kP, .kI = kI, .kD = kD, .error = 0.0,
                          .integral = 0.0, .output = 0.0, .integralLim = 50.0};
}

double pidControllerComputeOutput(PidController* pidController, double error) {
  if(!pidController) {
    return 0.0;
  }
  pidController->integral = (pidController->kI == 0) ? 0.0 : limUpTo(pidController->integralLim, pidController->integral + error);
  const double derivative = (pidController->kD == 0) ? 0.0 : (error - pidController->error);
  pidController->output = pidController->kP * error + pidController->kI * pidController->integral + pidController->kD * derivative;
  pidController->error = error;
  return pidController->output;
}

double pidControllerOutput(PidController* pidController) {
  return pidController->output;
}

// LIFT MECH DEFINITIONS

#define LIFT_THRESH  30.0f

std::unique_ptr<pros::Motor> liftm;

PidController liftPid = pidControllerCreate(1.0, 0, 0.2);

bool lowMacroRollers = false;

namespace liftMech {

  void init() {
    liftm = std::make_unique<pros::Motor>(INTAKE_LIFT,
                                       pros::E_MOTOR_GEARSET_18, false);
  }

  void stop() { liftm->move(0); }

  void setLift(double pos) {
    double liftError = 139.0, liftPow;
    int t = 0;
    while(liftError > LIFT_THRESH) {
      liftError = pos - liftm->get_position();
      liftPow = maxSpeedLimit(127, pidControllerComputeOutput(&liftPid, liftError));
      liftm->move(liftPow);
      t++;
      if(t > 5000) break;
      pros::delay(10);
    }
  }

  void liftControl(void *param) {
    double curr;
    while(true) {

      if(master.getDigital(ControllerDigital::down)) {
        curr = 0;
      }

      if(master.getDigital(ControllerDigital::right)) {
        curr = 2900;//407.2;
      }

      if(master.getDigital(ControllerDigital::Y)) {
        curr = 2300.0;
      }

      if(master.getDigital(ControllerDigital::L1)) {
        liftm->move(127);
        while(master.getDigital(ControllerDigital::L1)) {
          pros::delay(10);
        }
        curr = liftm->get_position();
        liftm->move(0);
      }
      else if(master.getDigital(ControllerDigital::L2)) {
        liftm->move(-127);
        while(master.getDigital(ControllerDigital::L2)) {
          pros::delay(10);
        }
        curr = liftm->get_position();
        liftm->move(0);
      }

      if(liftm->get_position() < -10) {
        curr = -10;
      }

      //printf("Lift Pos: %lf\n", liftm->get_position());
      liftm->move(pidControllerComputeOutput(&liftPid, (curr - liftm->get_position())));
      //printf("power: %fl\n", pidControllerComputeOutput(&liftPid, (curr - liftm->getPosition())));
      pros::delay(20);
    }
  }
}

// TRAY MECH DEFINITIONS

#define DISPENSE_SPEED  127
#define RETRACT_SPEED   -127
#define STARTING_POS    0
#define ENDING_POS      2327.00000
#define TRAY_ERROR_THRESH   20
#define IS_LIFTED_THRESH    800
#define MIN_POS_CLEARANCE   550.6

std::unique_ptr<pros::Motor> traym;
std::unique_ptr<pros::ADIDigitalIn> trayButt;

PidController trayPid = pidControllerCreate(1.0, 0, 0.2);
PidController trayPidDispense = pidControllerCreate(.09, 0, 0.2);
PidController trayPidAutoDispense = pidControllerCreate(.2, 0, 0.2);


bool trayDown = true;
bool dispensing = false;

namespace trayMech {
  void init() {
    traym = std::make_unique<pros::Motor>(TRAY,
                                       pros::E_MOTOR_GEARSET_18, true, pros::E_MOTOR_ENCODER_DEGREES);

    trayButt = std::make_unique<pros::ADIDigitalIn>('D');

  }

  void stop() { traym->move(0); }

  void dispense() {
    double trayError = 139.0, trayPow;
    int t = 0;
    dispensing = true;
    while(fabs(trayError) > TRAY_ERROR_THRESH) {
      trayError = ENDING_POS - traym->get_position();
      trayPow = maxSpeedLimit(DISPENSE_SPEED, pidControllerComputeOutput(&trayPidDispense, trayError));
      //printf("%fl\n", trayPow);
      traym->move(trayPow+10);
      t++;
      if (t > 5000) break;
      pros::delay(20);
    }
    intakeSpeed = 6000;
    dispensing = false;
  }

  void dispenseAuton() {
    double trayError = 139.0, trayPow;
    int t = 0;
    dispensing = true;
    while(fabs(trayError) > TRAY_ERROR_THRESH) {
      trayError = ENDING_POS - traym->get_position();
      trayPow = maxSpeedLimit(DISPENSE_SPEED, pidControllerComputeOutput(&trayPidAutoDispense, trayError));
      //printf("%fl\n", trayPow);
      traym->move(trayPow+10);
      t++;
      if (t > 5000) break;
      pros::delay(20);
    }
    intakeSpeed = 6000;
    dispensing = false;
  }

  void retract() {
    double trayError = 139.0, trayPow;
    int t = 0;
    intakeSpeed = 12000;

    while(fabs(trayError) > TRAY_ERROR_THRESH || trayButt->get_value()) {
      trayError = STARTING_POS - traym->get_position();
      trayPow = maxSpeedLimit(RETRACT_SPEED, pidControllerComputeOutput(&trayPid, trayError));
      //printf("%fl\n", trayPow);
      traym->move(trayPow);
      t++;
      if (t > 6000) break;
      pros::delay(20);
    }
  }

  void retractBeginning() {
    double trayError = 139.0, trayPow;
    int t = 0;
    intakeSpeed = 12000;

    while(fabs(trayError) > TRAY_ERROR_THRESH || trayButt->get_value()) {
      trayError = - ENDING_POS + traym->get_position();
      trayPow = maxSpeedLimit(RETRACT_SPEED, pidControllerComputeOutput(&trayPid, trayError));
      //printf("%fl\n", trayPow);
      traym->move(trayPow);
      t++;
      if (t > 100) break;
      pros::delay(20);
    }
  }

  void trayControl(void *param) {
    double curr;
    //traym->tare_position();
    while(!pros::competition::is_autonomous()) {
      //printf("TRAY POS: %fl\n", traym->get_position());
      if(master.getDigital(ControllerDigital::B)) {
        if(trayDown) {
          dispensing = true;
          dispense();
          dispensing = false;
          curr = ENDING_POS;
        }
        else {
          retract();
          curr = STARTING_POS;
        }
        trayDown = !trayDown;
      }


      if(master.getDigital(ControllerDigital::up)) {
        traym->move(-60);
        while(master.getDigital(ControllerDigital::up)) {
          pros::delay(10);
        }
        traym->tare_position();
        curr = 0;
        traym->move(0);
      }
      if(master.getDigital(ControllerDigital::X)) {
        //printf("reached\n");
        //curr = ENDING_POS;
        retractBeginning();
        curr = traym->get_position();
        traym->tare_position();
        traym->move(0);
      }
      else if(master.getDigital(ControllerDigital::down)) {
        traym->tare_position();
        curr = 0;
      }

/*
      if(liftm->get_position() > IS_LIFTED_THRESH) {
        if(traym->get_position() < MIN_POS_CLEARANCE) {
          curr = MIN_POS_CLEARANCE;
        }
      }
      else if(trayDown && liftm->get_position() < (IS_LIFTED_THRESH + 40)) {
        if(traym->get_position() > MIN_POS_CLEARANCE-20) {
          curr = STARTING_POS;
        }
      }
      */
      //printf("TrayPos: %lf\n", traym->get_position());
      //printf("Dispense state: %d\n", trayDown);
      traym->move(pidControllerComputeOutput(&trayPid, (curr - traym->get_position())));
      //printf("power: %fl\n", pidControllerComputeOutput(&trayPid, (curr - traym->getPosition())));
      pros::delay(20);
    }
  }
}
