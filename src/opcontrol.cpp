#include "main.h"

okapi::Controller master;

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */


void opcontrol() {

	okapi::ControllerButton buttonUp = master[okapi::ControllerDigital::up];
  okapi::ControllerButton buttonDown = master[okapi::ControllerDigital::down];
  okapi::ControllerButton buttonLeft = master[okapi::ControllerDigital::left];
  okapi::ControllerButton buttonRight = master[okapi::ControllerDigital::right];
  okapi::ControllerButton buttonL1 = master[okapi::ControllerDigital::L1];
  okapi::ControllerButton buttonL2 = master[okapi::ControllerDigital::L2];
  okapi::ControllerButton buttonL2Copy = master[okapi::ControllerDigital::L2];
  okapi::ControllerButton buttonR1 = master[okapi::ControllerDigital::R1];
  okapi::ControllerButton buttonR2 = master[okapi::ControllerDigital::R2];
  okapi::ControllerButton buttonX = master[okapi::ControllerDigital::X];
  okapi::ControllerButton buttonY = master[okapi::ControllerDigital::Y];
  okapi::ControllerButton buttonA = master[okapi::ControllerDigital::A];
  okapi::ControllerButton buttonB = master[okapi::ControllerDigital::B];

	pros::Task intakeController (roller::intakeControl, (void*)"PROS", TASK_PRIORITY_DEFAULT,
                TASK_STACK_DEPTH_DEFAULT, "Intake Control Task");

	pros::Task LCDController (display, (void*)"PROS", TASK_PRIORITY_DEFAULT,
								TASK_STACK_DEPTH_DEFAULT, "Intake Control Task");

	pros::Task LiftController (liftMech::liftControl, (void*)"PROS", TASK_PRIORITY_DEFAULT,
								TASK_STACK_DEPTH_DEFAULT, "Lift Control Task");

	pros::Task TrayController (trayMech::trayControl, (void*)"PROS", TASK_PRIORITY_DEFAULT,
								TASK_STACK_DEPTH_DEFAULT, "Lift Control Task");
/*
	pros::Task OdomCalculations (odom::trackPositionTask, (void*)"PROS", TASK_PRIORITY_DEFAULT,
								TASK_STACK_DEPTH_DEFAULT, "Lift Control Task");
*/
	bool arcade = false;

	// tray control task
	// lift control task
	// odom task

	


	while(!pros::competition::is_autonomous()) {


		drive::userLR(master.getAnalog(okapi::ControllerAnalog::rightX), master.getAnalog(okapi::ControllerAnalog::leftY), 0);

		//printf("%lf\t%lf\t%lf\t%lf\n", r1->getPosition(), r2->getPosition(), l1->getPosition(), l2->getPosition());



		if(master.getDigital(okapi::ControllerDigital::left) && master.getDigital(okapi::ControllerDigital::L2)) {
			//pilons::moveToTargetSimple(24.0, 0.0, gPosition.y, gPosition.x, 70, 0, 0.5, 0, 0, 0, STOP_TYPE_HARSH, MTT_SIMPLE, false);
		}

/*
		if((master.getDigital(okapi::ControllerDigital::L2) && master.getDigital(okapi::ControllerDigital::B)) || (master.getDigital(okapi::ControllerDigital::L2) && master.getDigital(okapi::ControllerDigital::down))) {
			drive::setDrive(-85, -85);
			pros::delay(175);
			drive::setDrive(0,0);
			pros::delay(100);
			drive::setDrive(70,70);
			pros::delay(70);
			drive::setDrive(0,0);
			lowMacroRollers = true;
		}
*/
		//std::cout <<  odomChassis->getState().str() << "\n";
		//std::cout <<  LEnc->get() << " " << REnc->get() << "\n";
		std::cout <<  trayButt->get_value() << "\n";

		pros::delay(10);
	}


}
