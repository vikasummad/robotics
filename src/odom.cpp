#include "main.h"

using namespace okapi;


sPos gPosition;
sVel gVelocity;

////////////////////////////////////////////////////////
//              UTILITIES
////////////////////////////////////////////////////////


float toDeg(float rad) {
  return rad * 180 / PI;
}

float toRad(float deg) {
  return deg * PI / 180;
}

float nearAngle(float angle, float reference) {
  return std::round((reference - angle) / (2 * PI)) * (2 * PI) + angle;
}

float sgn(int val) {
  if(val < 0) return -1.0;
  return 1.0;
}

float limToVal(int input, int val) {
  if(abs(input) > val)
    return val * sgn(input);
  return input;
}

////////////////////////////////////////////////////////
//              MOTION PROFILE
////////////////////////////////////////////////////////








////////////////////////////////////////////////////////
//              ODOM
////////////////////////////////////////////////////////
namespace PosTrack {
  TwoWheelOdometry::TwoWheelOdometry(std::shared_ptr<ReadOnlyChassisModel> model, ChassisScales scales)
      : currentPose(0_m, 0_m, 0_rad)
      , model(model)
      , scales(scales) {}

  void TwoWheelOdometry::update() {
      auto newSensorValues = model->getSensorVals();

      double deltaLeft = (newSensorValues[0] - lastLeft) / scales.straight;
      double deltaRight = (newSensorValues[1] - lastRight) / scales.straight;

      lastLeft = newSensorValues[0];
      lastRight = newSensorValues[1];

      double deltaAvg = (deltaLeft + deltaRight) / 2.0;
      double deltaTheta = (deltaLeft - deltaRight) / scales.wheelTrack.convert(meter);

      double r1 = deltaTheta == 0 ? 0 : deltaAvg / deltaTheta;

      PositionVector positionUpdate(
          meter * (deltaTheta == 0 ? deltaAvg : (r1 * std::sin(deltaTheta))),
          meter * (deltaTheta == 0 ? 0 : (r1 - r1 * std::cos(deltaTheta))));

      // todo not be dumb
      positionUpdate.rotateSelf(currentPose.heading);
      positionUpdate.setSelf(PositionVector(positionUpdate.getX(), -positionUpdate.getY())); // this is gross fix asap
      currentPose.position.addSelf(positionUpdate);
      currentPose.turn(radian * deltaTheta);
  }

  Pose TwoWheelOdometry::getPose() {
      return currentPose;
  }

  void TwoWheelOdometry::setPose(Pose p) {
      currentPose = p;
  }

  void TwoWheelOdometry::printPose() {
      printf("X: %.2f | Y: %.2f | A: %.2f\n", currentPose.position.getX().convert(inch), currentPose.position.getY().convert(inch), currentPose.heading.convert(degree));
  }
}


/*
namespace odom {

  void init() {

    LEnc = std::make_unique<ADIEncoder>(ENC_LEFT_1, ENC_LEFT_2, true);
    REnc = std::make_unique<ADIEncoder>(ENC_RIGHT_1, ENC_RIGHT_2, false);
    MEnc = std::make_unique<ADIEncoder>(ENC_MID_1, ENC_MID_2, false);

    sPos gPosition = {.a = 0.0, .y = 0.0, .x = 0.0, .leftLast = 0, .rightLast = 0, .midLast = 0};
    sVel gVelocity = {.a = 0.0, .y = 0.0, .x = 0.0, .lastChecked = 0, .lastPosA = 0.0, .lastPosX = 0.0, .lastPosY = 0.0};

  }

  void trackPos(int left, int right, int mid, sPos* position) {
    float L = (left - position->leftLast) * SPIN_TO_IN_LR;
    float R = (right - position->rightLast) * SPIN_TO_IN_LR;
    float S = (mid - position->midLast) * SPIN_TO_IN_S;

    position->leftLast = left;
    position->rightLast = right;
    position->midLast = mid;

    float h; // hypotenuse travelled by robot center (starting pos, ending pos, center of circle travelled around)
    float i; // half angle travelled
    float h2; // same as h but using mid wheel
    float a = (L - R) / (L_DISTANCE_IN + R_DISTANCE_IN);

    if(a) {
      float r = R / a; // The radius of the circle the robot travel's around with the right side of the robot
      i = a / 2.0;
      float sinI = sin(i);
      h = ((r + R_DISTANCE_IN) * sinI) * 2.0;

      float r2 = S / a;
      h2 = ((r2 + S_DISTANCE_IN) * sinI) * 2.0;
    }

    else {
      h = R;
      i = 0;
      h2 = S;
    }

    float p = i + position->a;
    float cosP = cos(p);
    float sinP = sin(p);

    position->y += h * cosP;
    position->x += h * sinP;

    position->y += h2 * -sinP;
    position->x += h2 * cosP;

    position->a += a;

    //printf("p:%f   h:%f   h2:%f   i:%f   a:%f\n", p, h , h2, i, a);

  }

  void resetPos(sPos* position) {
    position->leftLast = 0;
    position->rightLast = 0;
    position->midLast = 0;
    position->x = 0;
    position->y = 0;
    position->a = 0;
  }

  void resetVel(sVel* velocity, sPos* position) {
    velocity->a = 0;
    velocity->x = 0;
    velocity->y = 0;

    velocity->lastPosA = 0;
    velocity->lastPosX = 0;
    velocity->lastPosY = 0;

    velocity->lastChecked = pros::millis();
  }

  void trackVel(sPos* position, sVel* velocity) {
    std::uint32_t currTime = pros::millis();
    std::uint32_t passed = currTime - velocity->lastChecked;
    if(passed > 40) {
      float posA = position->a;
      float posX = position->x;
      float posY = position->y;

      velocity->a = ((posA - velocity->lastPosA) * 1000.0) / passed;
      velocity->x = ((posX - velocity->lastPosX) * 1000.0) / passed;
      velocity->y = ((posY - velocity->lastPosY) * 1000.0) / passed;

      velocity->lastPosA = posA;
      velocity->lastPosX = posX;
      velocity->lastPosY = posY;
      velocity->lastChecked = currTime;
    }
  }

  void vectorToPolar(sVector* vector, sPolar* polar) {
  	if (vector->x || vector->y) {
  		polar->mag = sqrt(vector->x * vector->x + vector->y * vector->y);
  		polar->angle = atan2(vector->y, vector->x);
  	}
  	else
  		polar->mag = polar->angle = 0;
  }

  void polarToVector(sPolar* polar, sVector* vector) {
  	if (polar->mag) {
  		vector->x = polar->mag * cos(polar->angle);
  		vector->y = polar->mag * sin(polar->angle);
  	}
  	else
  		vector->x = vector->y = 0;
  }

  float getAngleOfLine(sLine* line) {
  	return atan2(line->p2.x - line->p1.x, line->p2.y - line->p1.y);
  }

  float getLengthOfLine(sLine* line) {
  	float x = line->p2.x - line->p1.x;
  	float y = line->p2.y - line->p1.y;
  	return sqrt(x * x + y * y);
  }

  void trackPositionTask(void* param) {
    while(true) {
      trackPos(LEnc->get(), REnc->get(), MEnc->get(), &gPosition);
      trackVel(&gPosition, &gVelocity);
      pros::delay(10);
    }
  }

  void resetPositionFull(sPos* position, float y, float x, float a) {
    // stop odom tasks

    resetPos(position);

    LEnc->reset();
    REnc->reset();
    MEnc->reset();

    position->a = a;
    position->x = x;
    position->y = y;

    // start task again
  }

}


sVector gTargetLast;


namespace pilons {

  bool getVelocity(float &out) {
    out = gVelocity.x * sin(gVelocity.a) + gVelocity.y * cos(gVelocity.a);
    return true;
  }

  bool TimedOut( uint32_t timeOut, float vel, uint32_t elspedTime, int* velSafetyCounter ) {
    float curVel;

    if(velSafetyCounter) {
      if((elspedTime > 400) && getVelocity(curVel) && (abs(curVel) < abs(vel) || sgn(vel) != sgn(curVel))) {
        *velSafetyCounter+=1;
      }
      else {
        *velSafetyCounter = 0;
      }
    }
    if(pros::millis() > timeOut || (velSafetyCounter && *velSafetyCounter >= 10 )) {
      printf("%d Exceeded Time %d, %f - ", pros::millis(), timeOut, curVel);
      return true;
    }
    else {
      printf("CurVel: %f\n", curVel);
      return false;
    }
  }

  void applyHarshStop() {
    sVector vel;
    vel.x = gVelocity.x;
    vel.y = gVelocity.y;
    sPolar polarVel;
    odom::vectorToPolar(&vel, &polarVel);
    polarVel.angle += gPosition.a;
    odom::polarToVector(&polarVel, &vel);
    float yPow = vel.y, aPow = gVelocity.a;

    printf("Vel y | a: %f | %f", yPow, aPow);

    yPow *= -0.7;
    aPow *= -6.3;

    float left = yPow + aPow;
    float right = yPow - aPow;

    left = sgn(left) * MAX(fabs(left), 7);
    right = sgn(right) * MAX(fabs(right), 7);

    left = limToVal(left, 30);
    right = limToVal(right, 30);

    printf("Applying harsh stop: %f %f", left, right);
    drive::setDrive(left, right);
    pros::delay(150);
    drive::setDrive(0, 0);
  }

  void moveToTargetSimple(float y, float x, float ys, float xs, float power, float startPower, float maxErrX, float decelEarly, float decelPower, float dropEarly, int stopType, int mode, bool velSafety) {
    int velSafetyCounter = 0;

    printf("Moving to (%f, %f) from (%f, %f) at %f\n", x, y, xs, ys, power);

    // Update last pos
    gTargetLast.y = y;
    gTargetLast.x = x;

    // Create line to follow
    sLine followLine;

    // Start Points
    followLine.p1.y = ys;
    followLine.p1.x = xs;

    // End Points
    followLine.p2.y = y;
    followLine.p2.x = x;

    float lineLen = odom::getLengthOfLine(&followLine);
    printf("Line length: %.2f\n", lineLen);

    float lineAngle = odom::getAngleOfLine(&followLine);
    printf("Line angle: %.2f\n", toDeg(lineAngle));

    float pidAngle = nearAngle(lineAngle - (power < 0 ? PI : 0), gPosition.a);
    printf("Pid angle: %.2f\n", toDeg(pidAngle));

    sVector currPosVector;
    sPolar currPosPolar;

    float vel;
    float sinLineAngle = sin(lineAngle);
    float cosLineAngle = cos(lineAngle);

    int last = startPower;
    float correction = 0;

    float multi;


    if(mode == MTT_SIMPLE) {
      drive::setDrive(power, power);
      printf("Set drive to %f\n", power);
    }

    int finalPower = power;

    std::uint32_t timeStart = pros::millis();

    do {
      currPosVector.x = gPosition.x - x;
      currPosVector.y = gPosition.y - y;
      odom::vectorToPolar(&currPosVector, &currPosPolar);
      currPosPolar.angle += lineAngle;
      odom::polarToVector(&currPosPolar, &currPosVector);

      printf("CurPosV: x=%f  y=%f   CurPosP: m=%f  a=%f\n", currPosVector.x, currPosVector.y, currPosPolar.mag, currPosPolar.angle);


      if(maxErrX) {
        float errA = gPosition.a - pidAngle;
        float errX = currPosVector.x + currPosVector.y * sin(errA) / cos(errA);
        float correctA = atan2(x - gPosition.x, y - gPosition.y);
        if(power > 0)
          correctA += PI;
        correction = fabs(errX) > maxErrX ? 8.0 * (nearAngle(correctA, gPosition.a) - gPosition.a) * sgn(power) : 0;
        printf("errA:%f | errX:%f | correctA:%f | correction:%f \n", errA, errX, correctA, correction);

      }

      if(mode != MTT_SIMPLE) {

        switch(mode) {
          case MTT_PROP:
            finalPower = std::round(-127.0 / 40.0 * currPosVector.y) * sgn(power);
            printf("PROP: finalPow: %d\n", finalPower);
            break;
          case MTT_CASC:

            const float kB = 2.8;
            const float kP = 2.0;

            // CUSTOMIZE CONSTANTS BASED ON AUTO MODE

            float vTarget = 45 * (1 - exp(0.07 * (currPosVector.y + dropEarly)));
            finalPower = round(kB * vTarget + kP * (vTarget - vel)) * sgn(power);
            printf("CASC: finalPow: %d\n", finalPower);

            break;
        }
        finalPower = limToVal(finalPower, abs(power));
        printf("LimPower: %d\n", finalPower);
        if(finalPower * sgn(power) < 30)
          finalPower = 30 * sgn(power);
        printf("AdjustedPow: %d\n", finalPower);

        int delta = finalPower - last;
        delta = limToVal(delta, 5);
        finalPower = last += delta;
        printf("AdjustedPow2: %d\n", finalPower);

        }
        switch((int)sgn(correction)) {
          case 0:
            drive::setDrive(finalPower, finalPower);
            printf("Set drive %d %d\n", finalPower, finalPower);
            break;
          case 1:
            drive::setDrive(finalPower,  finalPower * exp(-correction));
            printf("Set drive %d %f\n", finalPower, finalPower * exp(-correction));
          case -1:
            drive::setDrive(finalPower * exp(-correction), finalPower);
            printf("Set drive %lf %d\n", finalPower * exp(-correction), finalPower);
            break;
        }

        vel = sinLineAngle * gVelocity.x + cosLineAngle * gVelocity.y;

        if(stopType == STOP_TYPE_SOFT) multi = 1;
        else multi = 0;

        printf("vel: %f\n", vel);

        printf("Conditions: currPosVector.y = %f | dropEarly=%f | decelEarly=%f | velSafety=%d | timedOut=%d\n", currPosVector.y, dropEarly, decelEarly, velSafety, !TimedOut(pros::millis()+100, sgn(power) * 0.5, pros::millis()-timeStart, &velSafetyCounter));


      } while(currPosVector.y < -dropEarly - MAX( (vel * multi ? 0.175 : 0.098) , decelEarly) && (velSafety ? !TimedOut(pros::millis()+100, sgn(power) * 0.5, pros::millis()-timeStart, &velSafetyCounter) : 1) );

      // printf("%f, %f\n", currPosVector.y, vel);

      drive::setDrive(decelPower, decelPower);
      printf("\n\n//////////DECEL/////////////////\n\n");
      printf("Set drive %f %f\n", decelPower, decelPower);

      do
      {
        currPosVector.x = gPosition.x - x;
        currPosVector.y = gPosition.y - y;
        odom::vectorToPolar(&currPosVector, &currPosPolar);
        currPosPolar.angle += lineAngle;
        odom::polarToVector(&currPosPolar, &currPosVector);

        vel = sinLineAngle * gVelocity.x + cosLineAngle * gVelocity.y;
        printf("CurPosV: x=%f  y=%f   CurPosP: m=%f  a=%f\n", currPosVector.x, currPosVector.y, currPosPolar.mag, currPosPolar.angle);
        printf("vel: %f\n", vel);


      } while (currPosVector.y < -dropEarly - (vel * multi ? 0.175 : 0.098)) ;

      if(stopType == STOP_TYPE_SOFT) {
        drive::setDrive(-6 * sgn(power), -6 * sgn(power));
        printf("Set drive %f %f\n", -6 * sgn(power), -6 * sgn(power));
        do {
          currPosVector.x = gPosition.x - x;
          currPosVector.y = gPosition.y - y;
          odom::vectorToPolar(&currPosVector, &currPosPolar);
          currPosPolar.angle += lineAngle;
          odom::polarToVector(&currPosPolar, &currPosVector);

          vel = sinLineAngle * gVelocity.x + cosLineAngle * gVelocity.y;

          printf("CurPosV: x=%f  y=%f   CurPosP: m=%f  a=%f\n", currPosVector.x, currPosVector.y, currPosPolar.mag, currPosPolar.angle);
          printf("vel: %f\n", vel);

        } while (vel > 7 && currPosVector.y < 0);

      }

      if(stopType == STOP_TYPE_HARSH) {
        applyHarshStop();
        printf("applied harsh stop\n");
      }
      else {
        drive::setDrive(0,0);
        printf("Set drive %d %d\n", 0 , 0);

      }

      printf("Moved from (%f, %f) to (%f, %f) | %f  %f  %f\n", x, y, xs, ys, gPosition.x, gPosition.y, toDeg(gPosition.a));

  }

  void moveToTargetDisSimple(float a, float d, float ys, float xs, float power, float startPower, float maxErrX, float decelEarly, float decelPower, float dropEarly, int stopType, int mode, bool velSafety) {
    moveToTargetSimple(ys + d * cos(a), xs + d * sin(a), ys, xs, power, startPower, maxErrX, decelEarly, decelPower, dropEarly, stopType, mode, false);
  }


}
*/
