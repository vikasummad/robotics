#include "main.h"

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */

using namespace okapi;

void eightCubeBlueLong(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green, {{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {5_ft, 0_ft, 0_deg}},
    "A"
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2_ft, 1.4_ft, 0_deg}},
    "B", {0.45, 0.8, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {4.3_ft, 0_ft, 0_deg}},
    "C"
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.5_ft, 0_ft, 0_deg}},
    "D", {0.6, 0.9, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 0_ft, 0_deg}},
    "E", {0.6, 0.9, 5.0}
  );

  trayC->setTarget(1000);
  liftC->setTarget(1000);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  pros::delay(300);

  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  trayC->setTarget(0);
  liftC->setTarget(0);

/*

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("A");
  profileController->waitUntilSettled();

  intake_r->move_voltage(5000);
  intake_l->move_voltage(5000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("B", true, true);
  profileController->waitUntilSettled();

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  pros::delay(10);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("C");
  profileController->waitUntilSettled();

  intake_r->move(30);
  intake_l->move(30);

  trayC->setTarget(2000);

  driveChassis->getModel()->setMaxVelocity(100);

  turnChassis->turnAngle(140_deg);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("D");
  profileController->waitUntilSettled();

  trayC->setTarget(5000);
  trayC->waitUntilSettled();

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  profileController->setTarget("E", true);
  profileController->waitUntilSettled();

*/
/*
  intake_r->move_voltage(8000);
  intake_l->move_voltage(8000);

  profileController->setTarget("B", true);
  profileController->waitUntilSettled();

  pros::delay(10);

  //smoothTurn(1000, 60);

  turnChassis->turnAngle(-99_deg);

  profileController->setTarget("C");
  profileController->waitUntilSettled();

  drive::setDrivePowLR(-4000, 4000);
  pros::delay(100);
  drive::setDrivePowLR(0, 0);

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(300);

  intake_r->move(-30);
  intake_l->move(-30);

  trayMech::dispense();

  intake_r->move_voltage(-4000);
  intake_l->move_voltage(-4000);

  drive::setDrivePowLR(5000, -5000);
  pros::delay(250);
  drive::setDrivePowLR(0, 0);



  profileController->setTarget("D", true);
  profileController->waitUntilSettled();
  */
}

void eightCubeRedLong(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {5_ft, 0_ft, 0_deg}},
    "A"
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2_ft, 1.4_ft, 0_deg}},
    "B", {0.45, 0.8, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {4.3_ft, 0_ft, 0_deg}},
    "C"
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.5_ft, 0_ft, 0_deg}},
    "D", {0.6, 0.9, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 0_ft, 0_deg}},
    "E", {0.6, 0.9, 5.0}
  );

  trayC->setTarget(1000);
  liftC->setTarget(1000);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  pros::delay(300);

  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  trayC->setTarget(0);
  liftC->setTarget(0);

/*

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("A");
  profileController->waitUntilSettled();

  intake_r->move_voltage(5000);
  intake_l->move_voltage(5000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("B", true);
  profileController->waitUntilSettled();

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  pros::delay(10);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("C");
  profileController->waitUntilSettled();

  intake_r->move(30);
  intake_l->move(30);

  trayC->setTarget(2000);

  driveChassis->getModel()->setMaxVelocity(100);

  turnChassis->turnAngle(-140_deg);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("D");
  profileController->waitUntilSettled();

  trayC->setTarget(5000);
  trayC->waitUntilSettled();

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  profileController->setTarget("E", true);
  profileController->waitUntilSettled();

*/
/*
  intake_r->move_voltage(8000);
  intake_l->move_voltage(8000);

  profileController->setTarget("B", true);
  profileController->waitUntilSettled();

  pros::delay(10);

  //smoothTurn(1000, 60);

  turnChassis->turnAngle(-99_deg);

  profileController->setTarget("C");
  profileController->waitUntilSettled();

  drive::setDrivePowLR(-4000, 4000);
  pros::delay(100);
  drive::setDrivePowLR(0, 0);

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(300);

  intake_r->move(-30);
  intake_l->move(-30);

  trayMech::dispense();

  intake_r->move_voltage(-4000);
  intake_l->move_voltage(-4000);

  drive::setDrivePowLR(5000, -5000);
  pros::delay(250);
  drive::setDrivePowLR(0, 0);



  profileController->setTarget("D", true);
  profileController->waitUntilSettled();
  */
}



void redmain(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  //liftC->setTarget(0);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {7.0_ft, 0_ft, 0_deg}},
    "A", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.2_ft, 0_ft, 0_deg}},
    "B", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.2_ft, 0_ft, 0_deg}},
    "E", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.0_ft, 0_ft, 0_deg}},
    "F"
  );
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.7_ft, 0_ft, 0_deg}},
    "G", {0.5, 1.0, 5.0}
  );

    profileController->generatePath(
      {{0_ft, 0_ft, 0_deg},
      {0.5_ft, 0_ft, 0_deg}},
      "first move", {0.5, 1.0, 5.0}
    );

    liftC->setTarget(2800);

    trayC->setTarget(2500);
    profileController->setTarget("first move");
    liftC->waitUntilSettled();
    profileController->waitUntilSettled();
    profileController->removePath("first move");

    intake_r->move_voltage(-12000);
    intake_l->move_voltage(-12000);

    drive::setDrivePowLR(-9000, 9000);
    pros::delay(222);
    drive::setDrivePowLR(0, 0);

    drive::setDrivePowLR(-2000, 2000);

    intake_r->move_voltage(-12000);
    intake_l->move_voltage(-12000);

    trayC->setTarget(0);
    liftC->setTarget(0);

    pros::delay(500);

    intake_r->move_voltage(12000);
    intake_l->move_voltage(12000);

    drive::setDrivePowLR(0, 0);

  driveChassis->getModel()->setMaxVelocity(70);
  profileController->setTarget("A");

  liftC->waitUntilSettled();
  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("B", true);
  profileController->waitUntilSettled();

  turnChassis->turnAngle(-165_deg);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("E");
  profileController->waitUntilSettled();

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(300);

  intake_r->move(-30);
  intake_l->move(-30);

  trayC->setTarget(5500);
  trayC->waitUntilSettled();

  drive::setDrivePowLR(5000, -5000);
  pros::delay(250);
  drive::setDrivePowLR(0, 0);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("F", true);
  profileController->waitUntilSettled();

}

void bluemain(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  //liftC->setTarget(0);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {7.0_ft, 0_ft, 0_deg}},
    "A", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.2_ft, 0_ft, 0_deg}},
    "B", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.2_ft, 0_ft, 0_deg}},
    "E", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.0_ft, 0_ft, 0_deg}},
    "F"
  );
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.7_ft, 0_ft, 0_deg}},
    "G", {0.5, 1.0, 5.0}
  );

    profileController->generatePath(
      {{0_ft, 0_ft, 0_deg},
      {0.5_ft, 0_ft, 0_deg}},
      "first move", {0.5, 1.0, 5.0}
    );

    liftC->setTarget(2800);

    trayC->setTarget(2500);
    profileController->setTarget("first move");
    liftC->waitUntilSettled();
    profileController->waitUntilSettled();
    profileController->removePath("first move");

    intake_r->move_voltage(-12000);
    intake_l->move_voltage(-12000);

    drive::setDrivePowLR(-9000, 9000);
    pros::delay(222);
    drive::setDrivePowLR(0, 0);

    drive::setDrivePowLR(-2000, 2000);

    intake_r->move_voltage(-12000);
    intake_l->move_voltage(-12000);

    trayC->setTarget(0);
    liftC->setTarget(0);

    pros::delay(500);

    intake_r->move_voltage(12000);
    intake_l->move_voltage(12000);

    drive::setDrivePowLR(0, 0);

  driveChassis->getModel()->setMaxVelocity(70);
  profileController->setTarget("A");

  liftC->waitUntilSettled();
  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("B", true);
  profileController->waitUntilSettled();

  turnChassis->turnAngle(155_deg);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("E");
  profileController->waitUntilSettled();

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(300);

  intake_r->move(-30);
  intake_l->move(-30);

  trayC->setTarget(5500);
  trayC->waitUntilSettled();

  drive::setDrivePowLR(5000, -5000);
  pros::delay(250);
  drive::setDrivePowLR(0, 0);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("F", true);
  profileController->waitUntilSettled();
}

void masspickupRED() {

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.0003, 0, 0}).withMaxVelocity(100).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.75_ft, 0_ft, 0_deg}},
    "first move", {0.5, 1.0, 5.0}
  );

  liftC->setTarget(2900);

  trayC->setTarget(2500);
  profileController->setTarget("first move");
  liftC->waitUntilSettled();
  profileController->waitUntilSettled();
  pros::delay(300);
  profileController->removePath("first move");

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  drive::setDrivePowLR(-9000, 9000);
  pros::delay(222);
  drive::setDrivePowLR(0, 0);

  drive::setDrivePowLR(-2000, 2000);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  trayC->setTarget(0);
  liftC->setTarget(0);

  pros::delay(500);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  drive::setDrivePowLR(0, 0);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {9.0_ft, 0_ft, 0_deg}},
    "first three", {1.0, 1.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(70);
  profileController->setTarget("first three");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {27.0_ft, 0_ft, 0_deg}},
    "mass pickup", {1.0, 1.0, 5.0}
  );

  liftC->waitUntilSettled();
  profileController->waitUntilSettled();
  profileController->removePath("first three");

  pros::delay(250);

  turnChassis->turnAngle(100_deg);

  driveChassis->getModel()->setMaxVelocity(40);


  profileController->setTarget("mass pickup");

  profileController->waitUntilSettled();
  profileController->removePath("mass pickup");





}

void masspickupBLUE() {

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.0003, 0, 0}).withMaxVelocity(100).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.75_ft, 0_ft, 0_deg}},
    "first move", {0.5, 1.0, 5.0}
  );

  liftC->setTarget(2900);

  trayC->setTarget(2500);
  profileController->setTarget("first move");
  liftC->waitUntilSettled();
  profileController->waitUntilSettled();
  pros::delay(300);
  profileController->removePath("first move");

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  drive::setDrivePowLR(-9000, 9000);
  pros::delay(222);
  drive::setDrivePowLR(0, 0);

  drive::setDrivePowLR(-2000, 2000);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  trayC->setTarget(0);
  liftC->setTarget(0);

  pros::delay(500);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  drive::setDrivePowLR(0, 0);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {9.0_ft, 0_ft, 0_deg}},
    "first three", {1.0, 1.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(70);
  profileController->setTarget("first three");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {27.0_ft, 0_ft, 0_deg}},
    "mass pickup", {1.0, 1.0, 5.0}
  );

  liftC->waitUntilSettled();
  profileController->waitUntilSettled();
  profileController->removePath("first three");

  pros::delay(250);

  turnChassis->turnAngle(-100_deg);

  driveChassis->getModel()->setMaxVelocity(40);


  profileController->setTarget("mass pickup");

  profileController->waitUntilSettled();
  profileController->removePath("mass pickup");





}

void opPROG() {
  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.0003, 0, 0}).withMaxVelocity(100).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg}},
    "first move", {0.5, 1.0, 5.0}
  );

  liftC->setTarget(2800);

  trayC->setTarget(2500);

  profileController->setTarget("first move");
  profileController->waitUntilSettled();
  profileController->removePath("first move");

  liftC->waitUntilSettled();


  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  pros::delay(1000);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  drive::setDrivePowLR(-9000, 9000);
  pros::delay(222);
  drive::setDrivePowLR(0, 0);

  drive::setDrivePowLR(-2000, 2000);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  trayC->setTarget(0);
  liftC->setTarget(0);

  pros::delay(500);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  drive::setDrivePowLR(0, 0);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {5.0_ft, 0_ft, 0_deg}},
    "first five", {1.0, 1.0, 5.0}
  );

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  driveChassis->getModel()->setMaxVelocity(120);
  profileController->setTarget("first five");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.2_ft, 0.2_ft, 45_deg}},
    "curve around post", {0.1, 0.25, 2.5}
  );

  profileController->waitUntilSettled();
  profileController->removePath("first five");

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("curve around post", false, true);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.7_ft, 0_ft, 0_deg}},
    "get sixth", {0.1, 0.25, 2.5}
  );

  profileController->waitUntilSettled();
  profileController->removePath("curve around post");

  profileController->setTarget("get sixth");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.2_ft, 0.3_ft, 35_deg}},
    "exit post", {0.05, 0.25, 2.5}
  );

  profileController->waitUntilSettled();

  profileController->setTarget("exit post", false, false);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {4.7_ft, 0_ft, 0_deg}},
    "curve to goal", {1.0, 1.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("exit post");

  driveChassis->getModel()->setMaxVelocity(80);

  profileController->setTarget("curve to goal", false, true);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.3_ft, 0_ft, 0_deg}},
    "approach goal", {1.0, 1.0, 5.0}
  );

  profileController->waitUntilSettled();

  intake_r->move(30);
  intake_l->move(30);

  profileController->removePath("curve to goal");

  driveChassis->getModel()->setMaxVelocity(40);

  turnChassis->turnAngle(-62_deg);

  driveChassis->getModel()->setMaxVelocity(120);

  profileController->setTarget("approach goal");
  profileController->waitUntilSettled();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.5_ft, 0_ft, 0_deg}},
    "backup from stack", {1.0, 1.0, 5.0}
  );

  pros::delay(500);

  intake_r->move(30);
  intake_l->move(30);

  drive::setDrivePowLR(0, -5000);
  pros::delay(250);
  drive::setDrivePowLR(5000, 0);
  pros::delay(250);
  drive::setDrivePowLR(0, 0);

  trayC->setTarget(5900);
  trayC->waitUntilSettled();

  pros::delay(6000);

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(250);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("backup from stack", true);
  profileController->waitUntilSettled();


  trayC->setTarget(0);
  trayC->waitUntilSettled();

  trayC->flipDisable();

  auto fastTray = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.0005, 0, 0}).withMaxVelocity(100).build();

  turnChassis->getModel()->setMaxVelocity(50);

  turnChassis->turnAngle(-160_deg);

  turnChassis->getModel()->setMaxVelocity(100);


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3_ft, 0_ft, 0_deg}},
    "fence align", {1.0, 1.0, 5.0}
  );

  profileController->setTarget("fence align", true);
  profileController->waitUntilSettled();


  drive::setDrivePowLR(-5000, 5000);
  pros::delay(500);
  drive::setDrivePowLR(0, 0);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.7_ft, 0_ft, 0_deg}},
    "approach first tower", {1.0, 1.0, 5.0}
  );

  intake_l->move_voltage(7000);
  intake_r->move_voltage(7000);

  profileController->setTarget("approach first tower");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.2_ft, 0_ft, 0_deg}},
    "backup first tower", {1.0, 1.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("approach first tower");


  while((intake_l->get_torque() < 0.25 || intake_r->get_torque() < 0.25)) {
    intake_l->move_voltage(7000);
    intake_r->move_voltage(7000);

    printf("L: %lf \t R:%lf\n", intake_l->get_torque(), intake_r->get_torque());
    pros::delay(5);
  }

  intake_l->move_voltage(0);
  intake_r->move_voltage(0);

  liftC->waitUntilSettled();

  liftC->setTarget(2200);
  fastTray->setTarget(2500);
  profileController->setTarget("backup first tower", true);

  profileController->waitUntilSettled();
  profileController->removePath("backup first tower");


  liftC->waitUntilSettled();

  intake_l->move_voltage(-7000);
  intake_r->move_voltage(-7000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.7_ft, 0_ft, 0_deg}},
    "first to second tower", {1.0, 1.0, 5.0}
  );

  pros::delay(1500);

  liftC->setTarget(0);

  profileController->setTarget("first to second tower", true);
  profileController->waitUntilSettled();
  profileController->removePath("first to second tower");


  driveChassis->getModel()->setMaxVelocity(100);

  turnChassis->turnAngle(-100_deg);

  intake_l->move_voltage(7000);
  intake_r->move_voltage(7000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.0_ft, 0_ft, 0_deg}},
    "approach second tower", {1.0, 1.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("approach second tower");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg}},
    "last cube pickup", {1.0, 2.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("approach second tower");


  while((intake_l->get_torque() < 0.25 || intake_r->get_torque() < 0.25)) {
    intake_l->move_voltage(7000);
    intake_r->move_voltage(7000);

    printf("L: %lf \t R:%lf\n", intake_l->get_torque(), intake_r->get_torque());
    pros::delay(5);
  }

  intake_l->move_voltage(0);
  intake_r->move_voltage(0);

  liftC->waitUntilSettled();
  fastTray->waitUntilSettled();

  liftC->setTarget(2700);
  fastTray->setTarget(2500);

  liftC->waitUntilSettled();

  intake_l->move_voltage(-7000);
  intake_r->move_voltage(-7000);

  pros::delay(1500);

  liftC->setTarget(0);
  liftC->waitUntilSettled();

  intake_l->move_voltage(0);
  intake_r->move_voltage(0);

  turnChassis->getModel()->setMaxVelocity(40);

  turnChassis->turnAngle(110_deg);

turnChassis->getModel()->setMaxVelocity(200);

  intake_l->move_voltage(7000);
  intake_r->move_voltage(7000);

  profileController->setTarget("last cube pickup");
  profileController->waitUntilSettled();
  profileController->removePath("last cube pickup");

  while((intake_l->get_torque() < 0.25 || intake_r->get_torque() < 0.25)) {
    intake_l->move_voltage(7000);
    intake_r->move_voltage(7000);

    printf("L: %lf \t R:%lf\n", intake_l->get_torque(), intake_r->get_torque());
    pros::delay(5);
  }

  intake_l->move_voltage(0);
  intake_r->move_voltage(0);

  turnChassis->getModel()->setMaxVelocity(40);

  turnChassis->turnAngle(95_deg);

  turnChassis->getModel()->setMaxVelocity(200);

  liftC->setTarget(2200);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.8_ft, 0_ft, 0_deg}},
    "approach third tower", {1.0, 1.0, 5.0}
  );

  profileController->setTarget("approach third tower");
  profileController->waitUntilSettled();
  profileController->removePath("approach third tower");

  liftC->waitUntilSettled();

  intake_l->move_voltage(-7000);
  intake_r->move_voltage(-7000);

  pros::delay(1500);

  drive::setDrivePowLR(-7000, 7000);
  pros::delay(500);
  drive::setDrivePowLR(0, 0);

  pros::delay(1500);
}



//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////




void eightCubeBlue(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {5.0_ft, 0_ft, 0_deg}},
    "first three", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg},
    {2.8_ft, 1.7_ft, -5_deg}},
    "s back", {0.45, 0.8, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {4.0_ft, 0_ft, 0_deg}},
    "next four", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.7_ft, 0_ft, 0_deg}},
    "back to zone", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.3_ft, 0_ft, 0_deg}},
    "approach", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.6_ft, 0_ft, 0_deg}},
    "F"
  );
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.4_ft, 0_ft, 0_deg}},
    "G", {1.0, 1.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(130);

  profileController->setTarget("first three");
  pros::delay(750);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);
  profileController->waitUntilSettled();

  intake_r->move(30);
  intake_l->move(30);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("s back", true);
  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(130);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->setTarget("next four");
  profileController->waitUntilSettled();

  pros::delay(400);

  intake_r->move(30);
  intake_l->move(30);

  trayC->setTarget(3000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("back to zone", true);
  profileController->waitUntilSettled();

  turnChassis->turnAngle(133_deg);

  profileController->setTarget("approach");
  profileController->waitUntilSettled();

  intake_r->move_voltage(-3000);
  intake_l->move_voltage(-3000);

  trayC->setTarget(6000);
  trayC->waitUntilSettled();

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  profileController->setTarget("approach", true);
  profileController->waitUntilSettled();
}

void eightCubeRed(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                     .withClosedLoopControllerTimeUtil(0, 0, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  //auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.0007, 0, 0}).withMaxVelocity(100).build();

  liftC->setTarget(-50);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {5.0_ft, 0_ft, 0_deg}},
    "first three", {1.0, 2.5, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(130);

  profileController->setTarget("first three");

  pros::delay(750);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg},
    {2.8_ft, 1.35_ft, -5_deg}},
    "s back", {0.6, 2.2, 5.0}
  );


  profileController->waitUntilSettled();
  profileController->removePath("first three");

  intake_r->move(127);
  intake_l->move(127);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("s back", true, true);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {4.5_ft, 0_ft, 0_deg}},
    "next four", {1.0, 2.5, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("s back");


  driveChassis->getModel()->setMaxVelocity(130);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->setTarget("next four");
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.9_ft, 0_ft, 0_deg}},
    "back to zone", {1.1, 2.7, 5.0}
  );
  profileController->waitUntilSettled();
  profileController->removePath("next four");

  pros::delay(250);

  liftC->setTarget(-50);

  trayC->setTarget(2500);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("back to zone", true);
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.6_ft, 0_ft, 0_deg}},
    "approach", {1.0, 1.8, 5.0}
  );
  profileController->waitUntilSettled();
  profileController->removePath("back to zone");

  intake_r->move(30);
  intake_l->move(30);

  turnChassis->turnAngle(-133_deg);

  driveChassis->getModel()->setMaxVelocity(110);

  profileController->setTarget("approach");
  profileController->waitUntilSettled();

  intake_r->move_voltage(-2200);
  intake_l->move_voltage(-2200);

  trayC->setTarget(6200);
  trayC->waitUntilSettled();

  pros::delay(600);

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(500);

  driveChassis->getModel()->setMaxVelocity(60);

  profileController->setTarget("approach", true);
  profileController->waitUntilSettled();
  profileController->removePath("approach");
  liftC->waitUntilSettled();
}

void sevenCubeRed(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                     .withClosedLoopControllerTimeUtil(0, 0, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  //auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.0005, 0, 0}).withMaxVelocity(100).build();

  liftC->setTarget(-50);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.0_ft, 0_ft, 0_deg}},
    "first three", {1.0, 2.5, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(130);

  profileController->setTarget("first three");

  pros::delay(400);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 2.0_ft, 0_deg},
    {1.9_ft, 2.0_ft, 0_deg}},
    "s back", {0.6, 2.2, 5.0}
  );


  profileController->waitUntilSettled();
  profileController->removePath("first three");

  intake_r->move(127);
  intake_l->move(127);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("s back", true, true);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {4.5_ft, 0_ft, 0_deg}},
    "next four", {1.0, 2.5, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("s back");


  driveChassis->getModel()->setMaxVelocity(120);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->setTarget("next four");
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.9_ft, 0_ft, 0_deg}},
    "back to zone", {1.1, 4.0, 5.0}
  );
  profileController->waitUntilSettled();
  profileController->removePath("next four");

  pros::delay(400);

  liftC->setTarget(-50);

  trayC->setTarget(2000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("back to zone", true);
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.6_ft, 0_ft, 0_deg}},
    "approach", {1.0, 1.8, 5.0}
  );
  profileController->waitUntilSettled();
  profileController->removePath("back to zone");

  intake_r->move(30);
  intake_l->move(30);

  turnChassis->turnAngle(-133_deg);

  driveChassis->getModel()->setMaxVelocity(110);

  profileController->setTarget("approach");
  profileController->waitUntilSettled();

  intake_r->move_voltage(-2300);
  intake_l->move_voltage(-2300);

  trayC->setTarget(6600);
  trayC->waitUntilSettled();

  pros::delay(1300);

  intake_r->move_voltage(-2200);
  intake_l->move_voltage(-2200);

  pros::delay(600);

  driveChassis->getModel()->setMaxVelocity(60);

  profileController->setTarget("approach", true);

  intake_r->move_voltage(-7000);
  intake_l->move_voltage(-7000);

  profileController->waitUntilSettled();
  profileController->removePath("approach");
  liftC->waitUntilSettled();
}

void sevenCubeBlue(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                     .withClosedLoopControllerTimeUtil(0, 0, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  //auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.0005, 0, 0}).withMaxVelocity(100).build();

  liftC->setTarget(-50);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.0_ft, 0_ft, 0_deg}},
    "first three", {1.0, 2.5, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(130);

  profileController->setTarget("first three");

  pros::delay(400);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 2.0_ft, 0_deg},
    {1.9_ft, 2.0_ft, 0_deg}},
    "s back", {0.6, 2.2, 5.0}
  );


  profileController->waitUntilSettled();
  profileController->removePath("first three");

  intake_r->move(127);
  intake_l->move(127);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("s back", true);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {6.5_ft, 0_ft, 0_deg}},
    "next four", {1.0, 2.5, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("s back");


  driveChassis->getModel()->setMaxVelocity(100);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->setTarget("next four");
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.9_ft, 0_ft, 0_deg}},
    "back to zone", {1.1, 4.0, 5.0}
  );
  profileController->waitUntilSettled();
  profileController->removePath("next four");

  pros::delay(400);

  liftC->setTarget(-50);

  trayC->setTarget(2000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("back to zone", true);
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.0_ft, 0_ft, 0_deg}},
    "approach", {1.0, 1.8, 5.0}
  );
  profileController->waitUntilSettled();
  profileController->removePath("back to zone");

  intake_r->move(30);
  intake_l->move(30);

  turnChassis->turnAngle(133_deg);

  driveChassis->getModel()->setMaxVelocity(110);

  profileController->setTarget("approach");
  profileController->waitUntilSettled();

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  trayC->setTarget(6600);
  trayC->waitUntilSettled();

  pros::delay(1300);

  intake_r->move_voltage(-4000);
  intake_l->move_voltage(-4000);

  pros::delay(600);

  driveChassis->getModel()->setMaxVelocity(60);

  profileController->setTarget("approach", true);

  intake_r->move_voltage(-7000);
  intake_l->move_voltage(-7000);

  profileController->waitUntilSettled();
  profileController->removePath("approach");
  liftC->waitUntilSettled();
}

void curlRed() {

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                     .withClosedLoopControllerTimeUtil(0, 0, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  //auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.004, 0, 0}).withMaxVelocity(100).build();


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {5.2_ft, 0_ft, 0_deg}},
    "first three", {1.0, 2.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(130);

  profileController->setTarget("first three");

  pros::delay(500);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg}},
    "more", {1.0, 2.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("first three");

  drive::setDrivePowLR(0, -5000);
  pros::delay(5000);
  drive::setDrivePowLR(0, 0);

  profileController->setTarget("more");
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {5.0_ft, 0_ft, 0_deg}},
    "approach", {1.0, 2.0, 5.0}
  );
  profileController->waitUntilSettled();

  turnChassis->turnAngle(35_deg);

  profileController->setTarget("approach");
  profileController->waitUntilSettled();

  liftC->setTarget(-20);

  trayC->setTarget(6000);

  intake_r->move_voltage(-2000);
  intake_l->move_voltage(-2000);

  trayC->waitUntilSettled();

  intake_r->move_voltage(-10000);
  intake_l->move_voltage(-10000);
  profileController->setTarget("more", true);
  profileController->waitUntilSettled();

}

void curlBlue() {

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                     .withClosedLoopControllerTimeUtil(0, 0, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  //auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();



  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.004, 0, 0}).withMaxVelocity(100).build();


  liftC->setTarget(-20);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {7_ft, 0_ft, 0_deg}},
    "first three", {1.0, 2.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("first three");

  pros::delay(500);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg}},
    "more", {1.0, 2.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("first three");

  drive::setDrivePowLR(5000, 0);
  pros::delay(5000);
  drive::setDrivePowLR(0, 0);

  profileController->setTarget("more");
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {5.0_ft, 0_ft, 0_deg}},
    "approach", {1.0, 2.0, 5.0}
  );
  profileController->waitUntilSettled();

  turnChassis->turnAngle(-35_deg);

  profileController->setTarget("approach");
  profileController->waitUntilSettled();

  liftC->setTarget(-20);

  trayC->setTarget(6000);

  intake_r->move_voltage(-2000);
  intake_l->move_voltage(-2000);

  trayC->waitUntilSettled();

  intake_r->move_voltage(-10000);
  intake_l->move_voltage(-10000);
  profileController->setTarget("more", true);
  profileController->waitUntilSettled();

}

void release() {
  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                     .withClosedLoopControllerTimeUtil(0, 0, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  //auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).withGains({0.004, 0, 0}).withMaxVelocity(100).build();


  liftC->setTarget(-20);

  trayC->setTarget(6000);

  intake_r->move_voltage(-2000);
  intake_l->move_voltage(-2000);

  trayC->waitUntilSettled();

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg}},
    "more", {1.0, 2.0, 5.0}
  );

  intake_r->move_voltage(-10000);
  intake_l->move_voltage(-10000);
  profileController->setTarget("more", true);
  profileController->waitUntilSettled();

}

void sixcubered(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  liftC->setTarget(-20);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {7.0_ft, 0_ft, 0_deg}},
    "A", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.7_ft, 0_ft, 0_deg}},
    "B", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.6_ft, 0_ft, 0_deg}},
    "E", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 0_ft, 0_deg}},
    "F"
  );
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.7_ft, 0_ft, 0_deg}},
    "G", {0.5, 1.0, 5.0}
  );



  driveChassis->getModel()->setMaxVelocity(90);
  profileController->setTarget("A");

  pros::delay(300);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(100);


  turnChassis->turnAngle(30_deg);

  profileController->setTarget("F");
  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("B", true);
  profileController->waitUntilSettled();

  turnChassis->turnAngle(-190_deg);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("E");
  profileController->waitUntilSettled();

  liftC->setTarget(-50);

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(300);

  intake_r->move(-30);
  intake_l->move(-30);

  trayC->setTarget(6000);
  trayC->waitUntilSettled();

  drive::setDrivePowLR(5000, -5000);
  pros::delay(250);
  drive::setDrivePowLR(0, 0);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("F", true);
  profileController->waitUntilSettled();

  liftC->waitUntilSettled();

}

void sixxcubeblue(){

  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  liftC->setTarget(-20);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {7.0_ft, 0_ft, 0_deg}},
    "A", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.7_ft, 0_ft, 0_deg}},
    "B", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.6_ft, 0_ft, 0_deg}},
    "E", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 0_ft, 0_deg}},
    "F"
  );
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.7_ft, 0_ft, 0_deg}},
    "G", {0.5, 1.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(90);
  profileController->setTarget("A");

  pros::delay(400);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(100);


  turnChassis->turnAngle(-33_deg);

  driveChassis->getModel()->setMaxVelocity(70);

  profileController->setTarget("F");
  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("B", true);
  profileController->waitUntilSettled();

  turnChassis->turnAngle(180_deg);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("E");
  profileController->waitUntilSettled();

  liftC->setTarget(-50);

  intake_r->move_voltage(-5000);
  intake_l->move_voltage(-5000);

  pros::delay(300);

  intake_r->move(-30);
  intake_l->move(-30);

  trayC->setTarget(6000);
  trayC->waitUntilSettled();

  drive::setDrivePowLR(5000, -5000);
  pros::delay(250);
  drive::setDrivePowLR(0, 0);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("F", true);
  profileController->waitUntilSettled();

  liftC->waitUntilSettled();

}

void push() {
  drive::setDrivePowLR(12000, -12000);
  pros::delay(400);
  drive::setDrivePowLR(0, 0);
  drive::setDrivePowLR(-12000, 12000);
  pros::delay(400);
  drive::setDrivePowLR(0, 0);
}

void largeblue() {
  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));

                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.001, 0, 0},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {7.0_ft, 0_ft, 0_deg}},
    "A", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.7_ft, 0_ft, 0_deg}},
    "B", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.6_ft, 0_ft, 0_deg}},
    "E", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 0_ft, 0_deg}},
    "F"
  );
  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.7_ft, 0_ft, 0_deg}},
    "G", {0.5, 1.0, 5.0}
  );

  driveChassis->getModel()->setMaxVelocity(90);

  profileController->setTarget("B");

  pros::delay(500);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->waitUntilSettled();

  turnChassis->turnAngle(-160_deg);

  profileController->setTarget("B");
profileController->waitUntilSettled();

intake_r->move_voltage(-12000);
intake_l->move_voltage(-12000);

}








void progskilz() {
  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                    .withClosedLoopControllerTimeUtil(70, 10, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.005, 0, 0.0001},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();



  liftC->setTarget(-20);
  driveChassis->getModel()->setMaxVelocity(150);


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg}},
    "flip", {1.0, 1.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {5.5_ft, 0_ft, 0_deg}},
    "first four", {1.0, 1.0, 5.0}
  );

  profileController->setTarget("flip");
  profileController->waitUntilSettled();
  profileController->setTarget("flip", true);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);
  pros::delay(400);
  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  profileController->waitUntilSettled();

  liftC->setTarget(-20);

  driveChassis->getModel()->setMaxVelocity(90);

  profileController->setTarget("first four");

  pros::delay(300);
  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.0_ft, 0_ft, 10_deg},
    {5.0_ft, 1.0_ft, 0_deg},
    {16_ft, 1.0_ft, 0_deg}},
    "pass the tower", {1.0, 1.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("first four");
  turnChassis->turnAngle(20_deg);

  liftC->waitUntilSettled();

  liftC->setTarget(2300);

  pros::delay(200);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  pros::delay(150);

  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  liftC->waitUntilSettled();

  liftC->setTarget(2300);

  turnChassis->getModel()->setMaxVelocity(60);

  //profileController->setTarget("first tower approach");
  //profileController->waitUntilSettled();

  turnChassis->moveDistance(-0.7_ft);

  intake_r->move_voltage(-10000);
  intake_l->move_voltage(-10000);

  pros::delay(700);

  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  turnChassis->moveDistance(0.7_ft);

  liftC->setTarget(-20);

  driveChassis->getModel()->setMaxVelocity(90);
  turnChassis->turnAngle(-20_deg);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  driveChassis->getModel()->setMaxVelocity(60);

  profileController->setTarget("pass the tower");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.5_ft, 0_ft, 0_deg}},
    "backup for second tower", {2.0, 4.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("pass the tower");

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("backup for second tower", true);

  liftC->waitUntilSettled();

  liftC->setTarget(2300);

  pros::delay(200);

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  pros::delay(150);

  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.5_ft, 0_ft, 0_deg}},
    "towards first zone", {2.0, 4.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.3_ft, 0_ft, 0_deg}},
    "approach second tower", {2.0, 4.0, 5.0}
  );

  profileController->waitUntilSettled();
  profileController->removePath("backup for second tower");

  turnChassis->getModel()->setMaxVelocity(40);
  turnChassis->turnAngle(-92_deg);

  liftC->waitUntilSettled();

  liftC->setTarget(2300);

  turnChassis->getModel()->setMaxVelocity(50);

  turnChassis->moveDistance(-0.5_ft);

  intake_r->move_voltage(-6000);
  intake_l->move_voltage(-6000);

  pros::delay(800);

  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  turnChassis->moveDistance(0.5_ft);

  turnChassis->getModel()->setMaxVelocity(90);

  liftC->waitUntilSettled();

  liftC->setTarget(-20);

  turnChassis->getModel()->setMaxVelocity(40);

  turnChassis->turnAngle(70_deg);

  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("towards first zone");

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {2.5_ft, 0_ft, 0_deg}},
    "approach first zone", {2.0, 4.0, 5.0}
  );
  profileController->waitUntilSettled();
  profileController->removePath("towards first zone");

  turnChassis->getModel()->setMaxVelocity(40);
  turnChassis->turnAngle(-40_deg);


  driveChassis->getModel()->setMaxVelocity(100);

  profileController->setTarget("approach first zone");
  profileController->waitUntilSettled();
  profileController->removePath("approach first zone");


  intake_r->move(-50);
  intake_l->move(-50);

  pros::delay(200);

  intake_r->move(30);
  intake_l->move(30);

  trayC->setTarget(6300);

  pros::delay(300);

  intake_r->move_voltage(-3000);
  intake_l->move_voltage(-3000);

  trayC->waitUntilSettled();

  pros::delay(700);

  intake_r->move(-80);
  intake_l->move(-80);

  pros::delay(500);

  driveChassis->getModel()->setMaxVelocity(110);


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.4_ft, 0_ft, 0_deg}},
    "backup from first zone", {1.0, 2.0, 5.0}
  );

  profileController->setTarget("backup from first zone", true);
  trayC->waitUntilSettled();
  trayC->setTarget(0);

  profileController->waitUntilSettled();

  turnChassis->turnAngle(140_deg);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.0_ft, 0_ft, 0_deg}},
    "approach third tower", {1.0, 2.0, 5.0}
  );

  trayC->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(110);

  profileController->removePath("backup from first zone");

  intake_r->move_voltage(9000);
  intake_l->move_voltage(9000);

  profileController->setTarget("approach third tower");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.65_ft, 0_ft, 0_deg}},
    "nudge back", {1.0, 2.0, 5.0}
  );

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.45_ft, 0_ft, 0_deg}},
    "backtostack", {1.0, 2.0, 5.0}
  );



  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {0.3_ft, 0_ft, 0_deg}},
    "nudge forward", {1.0, 2.0, 5.0}
  );

  profileController->waitUntilSettled();

  while((intake_l->get_torque() < 0.2 || intake_r->get_torque() < 0.2)) {
    intake_l->move_voltage(9000);
    intake_r->move_voltage(9000);

    drive::setDrivePowLR(7000, -7000);

    printf("L: %lf \t R:%lf\n", intake_l->get_torque(), intake_r->get_torque());
    pros::delay(5);
  }


  drive::setDrivePowLR(0, 0);
  intake_l->move_voltage(0);
  intake_r->move_voltage(0);

  driveChassis->getModel()->setMaxVelocity(110);

  profileController->setTarget("nudge forward", true);
  profileController->waitUntilSettled();


  liftC->setTarget(3000);
  liftC->waitUntilSettled();
  liftC->setTarget(3000);

  profileController->setTarget("nudge back");
  profileController->waitUntilSettled();

  intake_r->move_voltage(-12000);
  intake_l->move_voltage(-12000);

  pros::delay(700);

  intake_r->move_voltage(0);
  intake_l->move_voltage(0);

  profileController->setTarget("backtostack", true);
  profileController->waitUntilSettled();

  turnChassis->setMaxVelocity(50);

  turnChassis->turnAngle(96_deg);

  intake_r->move_voltage(12000);
  intake_l->move_voltage(12000);

  liftC->setTarget(-20);

  turnChassis->moveDistance(0.5_ft);

  driveChassis->setMaxVelocity(100);


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {13_ft, 0_ft, 0_deg}},
    "intake next stack", {1.0, 2.0, 5.0}
  );

  profileController->setTarget("intake next stack");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {7.5_ft, 0_ft, 0_deg}},
    "approach second zone", {1.0, 2.0, 5.0}
  );

  profileController->waitUntilSettled();

  turnChassis->getModel()->setMaxVelocity(50);

  turnChassis->turnAngle(50_deg);

  profileController->setTarget("approach second zone");
  profileController->waitUntilSettled();


  intake_r->move(-50);
  intake_l->move(-50);

  pros::delay(200);

  intake_r->move(30);
  intake_l->move(30);

  trayC->setTarget(6300);

  pros::delay(300);

  intake_r->move_voltage(-3000);
  intake_l->move_voltage(-3000);

  trayC->waitUntilSettled();

  pros::delay(700);

  intake_r->move(-80);
  intake_l->move(-80);

  pros::delay(500);

  driveChassis->getModel()->setMaxVelocity(110);


  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.4_ft, 0_ft, 0_deg}},
    "backup from first zone", {1.0, 2.0, 5.0}
  );

  profileController->setTarget("backup from first zone", true);
  trayC->waitUntilSettled();
  trayC->setTarget(0);

  profileController->waitUntilSettled();
}

void redPREP() {
  auto profileController = AsyncMotionProfileControllerBuilder()
                              .withLimits({1.0, 2.0, 10.0})
                              .withOutput(driveChassis)
                              .buildMotionProfileController();

  auto turnChassis = ChassisControllerBuilder()
                     .withMotors({-DRIVE_LEFT_1, -DRIVE_LEFT_2}, {DRIVE_RIGHT_1, DRIVE_RIGHT_2})
                     //.withChassisControllerTimeUtilFactory(TimeUtilFactory::withSettledUtilParams(50, 5, 250_ms));
                    .withClosedLoopControllerTimeUtil(70, 10, 0_ms)
                     //.withGearset(AbstractMotor::GearsetRatioPair{ AbstractMotor::gearset::green, 7.0/5.0 })
                     .withDimensions(AbstractMotor::gearset::green,{{4_in, 9.625_in}, imev5GreenTPR})
                     .withMaxVelocity(80)
                     .withGains(
                       {0.005, 0, 0.0001},
                       {0.007, 0, 0.0001}
                     )
                     .build();

  auto trayC = AsyncPosControllerBuilder().withMotor(TRAY).build();

  auto liftC = AsyncPosControllerBuilder().withMotor(INTAKE_LIFT).build();

  liftC->setTarget(-20);

  driveChassis->getModel()->setMaxVelocity(110);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3_ft, 0_ft, 0_deg}},
    "deploy antitips", {1.0, 2.0, 5.0}
  );

  profileController->setTarget("deploy antitips");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {3.5_ft, 0_ft, 0_deg}},
    "intake first row", {1.0, 2.0, 5.0}
  );

  pros::delay(400);

  intake_l->move_voltage(12000);
  intake_r->move_voltage(12000);

  profileController->waitUntilSettled();

  intake_l->move_voltage(12000);
  intake_r->move_voltage(12000);

  pros::delay(400);

  driveChassis->getModel()->setMaxVelocity(70);

  turnChassis->turnAngle(30_deg);

  profileController->setTarget("intake first row");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {8.0_ft, 0_ft, 0_deg}},
    "back to second row", {1.0, 2.0, 5.0}
  );

  profileController->waitUntilSettled();

  profileController->setTarget("back to second row", true);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {4.0_ft, 0_ft, 0_deg}},
    "intake second row", {1.0, 2.0, 5.0}
  );

  profileController->waitUntilSettled();

  turnChassis->turnAngle(-35_deg);

  profileController->setTarget("intake second row");

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1.5_ft, 2.0_ft, 0_deg},
    {1.9_ft, 2.0_ft, 0_deg}},
    "s back", {0.6, 2.2, 5.0}
  );

  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(200);

  profileController->setTarget("s back", true, true);

  profileController->generatePath(
    {{0_ft, 0_ft, 0_deg},
    {1_ft, 0_ft, 0_deg},
    {6_ft, 0_ft, 0_deg}},
    "next four", {1.0, 2.5, 5.0}
  );

  profileController->waitUntilSettled();

  driveChassis->getModel()->setMaxVelocity(110);

  profileController->setTarget("next four");

  profileController->waitUntilSettled();
















}

void autonomous() {
  //sevenCubeRed();
  //sevenCubeBlue();
  //progskilz();

  //curlBlue();

  //redPREP();

  //curlRed();
  //release();
  sixcubered();
  //sixxcubeblue();
  //eightCubeBlue();

  //largeblue();


  //push();
}
