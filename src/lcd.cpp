#include "main.h"

using namespace pros;

bool center_pressed = true;
bool left_pressed = false;
bool right_pressed = false;


void display(void *param) {

  char a[100];
  char b[100];
  char c[100];
  char d[100];
  char e[100];
  char f[100];
  char g[100];
  char h[100];

  int butt = 0;
  int prevButt = 0;
  while(!competition::is_autonomous()) {

    if(center_pressed && !left_pressed && !right_pressed) butt = 0;
    else if(!center_pressed && left_pressed && !right_pressed) butt = 1;
    else if(!center_pressed && !left_pressed && right_pressed) butt = 2;

    if(prevButt != butt) {
      lcd::clear();
    }

    if(butt == 0) {
      // Center is pressed
      /*
      sprintf(a, "FR Drive   %.2lf    %.1lfC   %.1lf rpm", r1->getPosition(), r1->getTemperature(), r1->getActualVelocity());
      sprintf(b, "BR Drive   %.2lf    %.1lfC   %.1lf rpm", r2->getPosition(), r2->getTemperature(), r2->getActualVelocity());
      sprintf(c, "FL Drive   %.2lf    %.1lfC   %.1lf rpm", l1->getPosition(), l1->getTemperature(), l1->getActualVelocity());
      sprintf(d, "BL Drive   %.2lf    %.1lfC   %.1lf rpm", l2->getPosition(), l2->getTemperature(), l2->getActualVelocity());
      sprintf(e, "Tray       %.2lf    %.1lfC   %.1lf rpm", traym->getPosition(), traym->getTemperature(), liftm->getActualVelocity());
      sprintf(f, "Lift       %.2lf    %.1lfC   %.1lf rpm", liftm->getPosition(), liftm->getTemperature(), liftm->getActualVelocity());
      sprintf(g, "IntakeR   %.2lf    %.1lfC   %.1lf rpm", intake_r->getPosition(), intake_r->getTemperature(), intake_r->getActualVelocity());
      sprintf(h, "IntakeL   %.2lf    %.1lfC   %.1lf rpm", intake_l->getPosition(), intake_l->getTemperature(), intake_l->getActualVelocity());

      lcd::print(0, a);
      lcd::print(1, b);
      lcd::print(2, c);
      lcd::print(3, d);
      lcd::print(4, e);
      lcd::print(5, f);
      lcd::print(6, g);
      lcd::print(7, h);
*/
      lcd::print(0, a);
      lcd::print(1, b);

      /*
      sprintf(a, "L Enc: %lf", LEnc->get());
      sprintf(b, "R Enc: %lf", REnc->get());
      sprintf(c, "M Enc: %lf", MEnc->get());
      sprintf(d, "X: %f     Y:%f     A:%f", gPosition.x, gPosition.y, toDeg(gPosition.a));
      sprintf(e, "X: %f     Y:%f     A:%f", gVelocity.x, gVelocity.y, toDeg(gVelocity.a));

      lcd::print(0, a);
      lcd::print(1, b);
      lcd::print(2, c);
      lcd::print(3, d);
      lcd::print(4, e);
*/
    }
    else if(butt == 1) {



    }
    else if(butt == 2) {


    }
/*
    sprintf(a, "%d", roller::speed);
    lcd::print(0, a);
    master.setText(2, 0, a);

    sprintf(b, "%lf", traym->getPosition());
    lcd::print(1, b);

    sprintf(c, "%lf", liftm->getPosition());
    lcd::print(2, c);
*/
    pros::delay(10);
  }
}
