/*
Intake stuff
*/

#ifndef _INTAKE_HPP_
#define _INTAKE_HPP_

#include "api.h"
#include "okapi/api.hpp"

using namespace okapi;

extern std::unique_ptr<pros::Motor> intake_r, intake_l;
extern std::unique_ptr<pros::ADIAnalogIn> lineSense;

extern int intakeSpeed;

namespace roller {

  extern int highSpeed;

  extern int lowSpeed;

  extern int speed;

  void init();

  void stop();

  void in(double speed);

  void out(double speed);

  void intakeControl(void *param);

  // DEBUGGING

  double getVelocity();

  double getTempRight();

  double getTempLeft();

}


#endif
