/*
Robot Port Config
*/

#ifndef _ROBOT_HPP_
#define _ROBOT_HPP_

#include "okapi/api.hpp"

using namespace okapi;

/*
Controller
*/

extern Controller master;

/*
Motors
*/

// 1 is front, 2 is back

#define DRIVE_RIGHT_1   11//11
#define DRIVE_RIGHT_2   6//13
#define DRIVE_LEFT_1    12//20
#define DRIVE_LEFT_2    20//19

#define INTAKE_R        3//2
#define INTAKE_L        15//10
#define TRAY            5//12
#define INTAKE_LIFT     17//18

/*
Sensors
*/

#define ENC_LEFT_1 'G'
#define ENC_LEFT_2 'H'
#define ENC_RIGHT_1 'E'
#define ENC_RIGHT_2 'F'
#define LINE_SENSOR 'B'
#define GYRO_YAW 'g'

/*
Param
*/

extern bool arcade;

/* Macro Global Var */

extern bool lowMacroRollers;

#endif
