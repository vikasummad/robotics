/*
Drive stuff
*/


#ifndef _DRIVE_HPP_
#define _DRIVE_HPP_

#include "api.h"
#include "okapi/api.hpp"

using namespace okapi;


extern std::shared_ptr<ChassisController> driveChassis;
extern std::unique_ptr<Motor> r1, r2, l1, l2;
extern std::shared_ptr<OdomChassisController> odomChassis;

//extern std::shared_ptr<AsyncMotionProfileController> profileController;



namespace drive {



  void init();

  void stopAll();

  void stopTasks();

  void userLR(double iforwardSpeed, double iyaw, double ithreshold);

  void setDrive(int l, int r);

  void setBrakeMode(AbstractMotor::brakeMode imode);

  void setDrivePowLR(int L, int R);

  double powToVolts(float power);

  void setDrivePowAll(int l1p, int l2p, int r1p, int r2p);

  void drivePIDLR(float LTarg, float RTarg, int t);

  double getRightEnc();

  double getLeftEnc();

  void tareAllDrive();

  void setDriveMaxVoltage(int ilimit);

  void setDriveMaxCurrent(int ilimit);

  void straightDrive(double power);

  // DEBUGGING

  int32_t getLeftVoltage();

  int32_t getRightVoltage();

  int32_t getCurrentDraw();

  double getVelocity();

}

#endif
