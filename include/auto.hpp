/*
Auton Functions
*/

#ifndef _AUTO_HPP_
#define _AUTO_HPP_

#include "api.h"
#include "okapi/api.hpp"

using namespace okapi;


extern void pathGen(okapi::QLength, okapi::QLength, okapi::QAngle, bool reverse = false);
extern void gen(std::string name, okapi::QLength, okapi::QLength, okapi::QAngle);
extern void runGen(okapi::QLength, okapi::QLength, okapi::QAngle, bool reverse = false);
extern void run(std::string name, bool reverse = false);
extern void finishRun(std::string name);
extern void in(int power = 127);
extern void out(int power = 127);
extern void alignF(int wait, int power = 127);
extern void alignB(int wait, int power = 127);
extern void alignR(int wait, int power = 127);
extern void alignL(int wait, int power = 127);
extern void turn(okapi::QAngle angle, int maxVel = 75);
extern void profilePosTurn(double angle, bool reset = true, int velLimit = 75);
extern void posTracker(void *params);
extern void setChassisVel(int max);
extern void stopAutoDrive();
extern void startAutoDrive();
extern void reset();
extern void startMotionProfileController();
extern void moveAsync(okapi::QLength distance);
extern void waitForSettle();
extern void allDrive(float power);
extern void delayPC();
extern void genK(std::string name, okapi::QLength, okapi::QLength, okapi::QAngle, PathfinderLimits ilimits);









#endif
