/*
PID Controllers
*/

#ifndef _PID_HPP_
#define _PID_HPP_

#include "api.h"
#include "okapi/api.hpp"

using namespace okapi;

extern double maxSpeedLimit(double maxSpeed, double currSpeed);





// PID DEFINITIONS

typedef struct PidController {
  double kP;
  double kI;
  double kD;
  double error;
  double integral;
  double output;
  double integralLim;
} PidController;

extern PidController pidControllerCreate(double kP, double kI, double kD);

extern double pidControllerComputeOutput(PidController* pidController, double error);

extern double pidControllerOutput(PidController* pidController);


// LIFT MECH DEFINITIONS

extern std::unique_ptr<pros::Motor> liftm;

extern PidController liftPid;

namespace liftMech {


  void init();

  void stop();

  void liftControl(void *param);

}

extern std::unique_ptr<pros::Motor> traym;

extern std::unique_ptr<pros::ADIDigitalIn> trayButt;


extern PidController trayPid;

extern PidController trayPidDispense;

extern PidController trayPidAutoDispense;

extern bool trayDown;

extern bool dispensing;

namespace trayMech {

  void init();

  void stop();

  void dispense();

  void dispenseAuton();

  void retract();

  double computePID(double error);

  void trayControl(void *param);

}

#endif
