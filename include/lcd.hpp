/*
LCD stuff
*/

#ifndef _LCD_HPP_
#define _LCD_HPP_

#include "api.h"

extern bool center_pressed;
extern bool left_pressed;
extern bool right_pressed;

extern void display(void *param);

#endif
