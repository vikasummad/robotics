/*
odom
*/

#ifndef _ODOM_HPP_
#define _ODOM_HPP_

#include "api.h"
#include "okapi/api.hpp"

using namespace okapi;

#define WHEEL_DIAMETER_IN_LR 2.841f // 2.843 //  2.783f
#define WHEEL_DIAMETER_IN_S 2.841f // 2.843 //  2.783f

// The distance between the tracking wheels and the centre of the robot in inches
#define L_DISTANCE_IN 3.625f //6.8198
#define R_DISTANCE_IN 3.625f //6.8198
#define S_DISTANCE_IN 2.75f

// The number of tick per rotation of the tracking wheel
#define TICKS_PER_ROTATION 360.0f

// Used internally by trackPosition
#define SPIN_TO_IN_LR (WHEEL_DIAMETER_IN_LR * PI / TICKS_PER_ROTATION)
#define SPIN_TO_IN_S (WHEEL_DIAMETER_IN_S * PI / TICKS_PER_ROTATION)

typedef struct pos {
  float a;
  float y;
  float x;
  int leftLast;
  int rightLast;
  int midLast;
} sPos;

typedef struct vel {
  float a;
  float y;
  float x;
  std::uint32_t lastChecked;
  float lastPosA;
  float lastPosX;
  float lastPosY;
} sVel;

typedef struct vector {
  float y;
  float x;
} sVector;

typedef struct polar {
  float mag;
  float angle;
} sPolar;

typedef struct line {
  sVector p1;
  sVector p2;
} sLine;

extern std::unique_ptr<ADIEncoder> LEnc;
extern std::unique_ptr<ADIEncoder> REnc;
extern std::unique_ptr<ADIEncoder> MEnc;

extern sPos gPosition;
extern sVel gVelocity;



////////////////////////////////////////////////////////
//              MOTION PROFILE
////////////////////////////////////////////////////////



////////////////////////////////////////////////////////
//              UTILITIES
////////////////////////////////////////////////////////


extern float toDeg(float rad);

extern float toRad(float deg);

extern float nearAngle(float angle, float reference);

extern float sgn(int val);

extern float limToVal(int input, int val);

////////////////////////////////////////////////////////
//              ODOM
////////////////////////////////////////////////////////

class PositionVector {
    okapi::QLength x;
    okapi::QLength y;

public:
    PositionVector();
    PositionVector(okapi::QLength x, okapi::QLength y);
    PositionVector(okapi::QAngle theta);

    static PositionVector add(PositionVector a, PositionVector b);
    static PositionVector subtract(PositionVector a, PositionVector b);
    static PositionVector multiply(PositionVector a, double n);
    static PositionVector divide(PositionVector a, double n);
    static PositionVector limit(PositionVector a, okapi::QLength limit);
    static PositionVector rotate(PositionVector a, okapi::QAngle theta);
    static PositionVector normalize(PositionVector a, okapi::QLength unit = okapi::meter);
    static okapi::QLength distanceBetween(PositionVector a, PositionVector b);
    static okapi::QAngle angleBetween(PositionVector a, PositionVector b);
    static okapi::QLength dot(PositionVector a, PositionVector b);

    void setSelf(PositionVector other);
    void addSelf(PositionVector other);
    void subtractSelf(PositionVector other);
    void multiplySelf(double n);
    void divideSelf(double n);
    void limitSelf(okapi::QLength n);
    void rotateSelf(okapi::QAngle theta);
    void normalizeSelf(okapi::QLength unit = okapi::meter);
    void setX(okapi::QLength newX);
    void setY(okapi::QLength newY);

    // TODO make a bunch of statics, then layer these ontop
    okapi::QLength distanceTo(PositionVector other);
    okapi::QAngle angleTo(PositionVector other);
    okapi::QLength dot(PositionVector other);

    okapi::QLength getX();
    okapi::QLength getY();
    okapi::QAngle getTheta();
    okapi::QLength getMag();
    okapi::QLength getMagSquared();
};

struct Pose {
    okapi::QAngle heading;
    PositionVector position;

    Pose();
    Pose(okapi::QLength x, okapi::QLength y, okapi::QAngle heading = 0_deg);
    Pose(PositionVector position, okapi::QAngle heading = 0_deg);

    void setHeading(okapi::QAngle newHeading);

    void turn(okapi::QAngle headingChange); // TODO constrain this reee
};

namespace PosTrack {
class PoseEstimator {
public:
    virtual Pose getPose() = 0;
    virtual void setPose(Pose pose) = 0;
    virtual void update() = 0;
    virtual void printPose() = 0;
};

class TwoWheelOdometry : public PoseEstimator {
    Pose currentPose;
    std::shared_ptr<okapi::ReadOnlyChassisModel> model;
    okapi::ChassisScales scales;

    double lastLeft = 0.0;
    double lastRight = 0.0;

public:
    TwoWheelOdometry(
        std::shared_ptr<okapi::ReadOnlyChassisModel> model,
        okapi::ChassisScales scales);

    virtual Pose getPose() override;
    virtual void setPose(Pose pose) override;

    virtual void update() override;

    virtual void printPose() override;
};
}




namespace odom {

  void init();

  void trackPos(int left, int right, int mid, sPos* position);

  void resetPos(sPos* position);

  void resetVel(sVel* velocity, sPos* position);

  void trackVel(sPos* position, sVel* velocity);

  void vectorToPolar(sVector* vector, sPolar* polar);

  void polarToVector(sPolar* polar, sVector* vector);

  float getAngleOfLine(sLine* line);

  float getLengthOfLine(sLine* line);

  void trackPositionTask(void* param);

  void autoMotorSensorUpdate(void* param);

  void resetPositionFull(sPos* position, float y, float x, float a);

}

////////////////////////////////////////////////////////
//              MOTION PROFILE
////////////////////////////////////////////////////////

#define STOP_TYPE_NONE    1
#define STOP_TYPE_HARSH   2
#define STOP_TYPE_SOFT    3

#define MTT_SIMPLE    1
#define MTT_PROP      2
#define MTT_CASC      3

extern sVector gTargetLast;

namespace pilons {

  bool getVelocty(float &out);

  bool TimedOut(uint32_t timeOut, float vel, uint32_t elspedTime, int* velSafetyCounter);

  void applyHarshStop();

  void moveToTargetSimple(float y, float x, float ys, float xs, float power, float startPower, float maxErrX, float decelEarly, float decelPower, float dropEarly, int stopType, int mode, bool velSafety);

  void moveToTargetDisSimple(float a, float d, float ys, float xs, float power, float startPower, float maxErrX, float decelEarly, float decelPower, float dropEarly, int stopType, int mode, bool velSafety);



}

#endif
